<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario4 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio4">

    <?php $form = ActiveForm::begin(); 

        echo $form
                ->field($model, 'mes') // campo
                ->checkboxList($model->meses());  //como checkbox list
        echo $form
                ->field($model, 'dia') // campo
                ->listBox($model->dias(),[
                    "multiple" => "multiple" // para poder seleccionar mas de uno
                ]);  // como checkbos list
    ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio4 -->
