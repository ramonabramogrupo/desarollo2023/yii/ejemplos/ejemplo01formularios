<?php

/* @var $this yii\web\View */

$this->title = 'Ejemplo 10';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Ejemplo 10 - Trabajo con Formularios</h1>

        <p class="lead">Trabajo con Formularios activos y controles avanzados </p>
        <p class="lead">Trabajo con controles de extensiones externas (cargar componentes) </p>
    </div>
</div>
