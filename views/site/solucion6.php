<?php


echo \yii\widgets\DetailView::widget([
    "model" => $model,
    "attributes" => [
        [
          "attribute" => "Nombre",
          "format" => "raw",
          "value" => function($model){
                return \yii\helpers\Html::img(
                        "@web/imgs/" . $model->nombre,
                        ["class"=>"img-thumbnail"]
                );
          }
        ],
    ]
]);


