<?php

echo yii\widgets\DetailView::widget([
    "model" => $model,
    "attributes" =>[
        "diaTexto", // convierte de array a string en el modelo
        "mesTexto",
        /*[
            "attribute" => "Mes como texto",
            "value"=>function($model){
                return join(",",$model->mes);
            }
        ]*/ // realizar la conversion de array a string en la vista
    ]
]);

