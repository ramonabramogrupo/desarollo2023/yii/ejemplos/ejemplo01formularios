<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario5 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio5">

    <?php $form = ActiveForm::begin(); 

        echo $form
                ->field($model, 'nombre')
                ->textInput(["placeholder"=>"Introduce nombre"]);
               
        echo $form
                ->field($model, 'apellidos')
                ->textInput(["placeholder"=> "Introduce los dos apellidos"]);
        
        echo $form
                ->field($model, 'email')
                ->input("email");
        
        echo $form
                ->field($model, 'fecha')
                ->input("date");
        
        echo $form
                ->field($model, 'poblacion')
                ->dropDownList($model->poblacionesMostrar(),[
                    'prompt' => 'Escoge una poblacion',
                ]);
        
        echo $form 
                ->field($model, 'meses')
                ->checkboxList($model->mesesMostrar());
        
        
    ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio5 -->
