<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario3 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio3">

    <?php 
        $form = ActiveForm::begin(); 
        echo $form
                ->field($model, 'mes') // campo
                ->radioList($model->meses()); // radio Button List
        
        echo $form
                ->field($model, 'dia') // campo
                ->radioList($model->dias()); // radio Button List
                
    ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio3 -->
