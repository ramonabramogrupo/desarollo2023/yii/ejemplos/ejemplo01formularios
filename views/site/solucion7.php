<?php

echo \yii\widgets\DetailView::widget([
    "model" => $model,
    "attributes" =>[
        "fecha",
        "listarOpciones",
        "identificador",
        "nombre",
        "listarElementos",
        [
            "attribute" => "imagen",
            "format" => "raw",
            "value" => function($model){
                return \yii\helpers\Html::img(
                        "@web/imgs/" . $model->imagen,
                        ["class"=>"img-thumbnail"],
                );
            }
        ]
    ],
]);

