<?php

namespace app\models;

class Formulario6 extends \yii\base\Model{
    public $nombre;
    
    public function rules(): array {
        return [
            [['nombre'],'file',
                'skipOnEmpty' => false, // obligatorio seleccionas una imagen
                'extensions' =>'jpg,png' // extensiones permitidas
                ],
        ];
    }
    
    public function attributeLabels(): array {
        return [
            "nombre" => "Selecciona imagen"
        ];
    }
    
    public function subirArchivo(): bool{
        $this->nombre->saveAs('imgs/' . $this->nombre->name);
        return true;
    }
    
    
    /**
     * antes de validar cojo los archivos enviados y
     * los coloco en el modelo
     * 
     */
    public function beforeValidate(){
        $this->nombre= \yii\web\UploadedFile::getInstance($this, 'nombre');
        return true;
    }
    
    /**
     * despues de validar subo el archivo
     * 
     */
    public function afterValidate() {
        $this->subirArchivo();
        return true;
    }
}
