<?php

namespace app\models;

class Formulario4 extends \yii\base\Model{
    public array $mes=[];
    public array $dia=[];
    private string $mesTexto="";
    private string $diaTexto="";
    
    public function rules(): array {
        return [
            [['mes','dia'],'safe'],
        ];
    }
    
    public function attributeLabels(): array {
        return [
            "mes" => "Mes comienzo",
            "dia" => "Dia comienzo",
        ];
    }
    
    public function dias():array{
        return ['Lunes','Martes','Miercoles','Jueves','Viernes'];
    }
    
    public function meses():array{
        return[
            "enero" => "Enero",
            "febrero" => "Febrero",
            "marzo" => "Marzo",
            "abril" => "Abril",
            "mayo" => "Mayo",
            "junio" => "Junio",
            "julio" => "Julio",
            "agosto" => "Agosto",
            "septiembre" => "Septiembre",
            "octubre" => "Octubre",
            "noviembre" => "Noviembre",
            "diciembre" => "Diciembre"
        ];
    }
    
    public function getMesTexto(): string {
        return join(",", $this->mes);
    }

    public function getDiaTexto(): string {
        return join(",",$this->dia);
    }


    
}
