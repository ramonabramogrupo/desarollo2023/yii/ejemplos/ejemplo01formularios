<?php


namespace app\models;


class Formulario7 extends \yii\base\Model{
    public string $fecha="";
    public array $opciones=[];
    public string $identificador="";
    public string $nombre="";
    public array $elementos=[];
    public $imagen;
    private string $listarOpciones="";
    private string $listarElementos="";
    
    
    public function rules(): array {
        return [
            [['imagen'],'file',
                'skipOnEmpty' => false,
                'extensions'=>'jpg,png,jpeg'
            ],
            [['fecha','identificador','nombre','opciones','elementos'],'required']
        ];
    }
    
    public function attributeLabels(): array {
        return [
          "fecha" => "Fecha Entrada",
          "opciones" => "Opciones",
          "identificador" => "Identificador",
          "nombre" => "Nombre Completo",
          "elementos" => "Elementos disponibles",
          "imagen" => "Imagen"
        ];
    }
    
    public function opcionesDisponibles() :array{
        return[
            "tarjeta de red"=>"Tarjeta de red",
            "tarjeta de video"=>"Tarjeta de video",
            "tarjeta de sonido"=>"Tarjeta de sonido",
            "altavoces"=>"Altavoces",
        ];
    }
    
    public function elementosDisponibles():array{
        return [
          "opcion 1"=> "opcion 1",
          "opcion 2"=> "opcion 2",
          "opcion 3"=> "opcion 3",
        ];
    }
    
    public function getlistarOpciones():string{
        return join(",",$this->opciones);
    }
    
    public function getListarElementos(): string {
        return join(",",$this->elementos);
    }
    
    public function beforeValidate() {
        $this->imagen= \yii\web\UploadedFile::getInstance($this, 'imagen');
        return true;
    }
    
    public function afterValidate() {
        $this->subirArchivo();
        return true;
    }
    
    public function subirArchivo(){
        $this->imagen->saveAs('imgs/' . $this->imagen->name);
        return true;
    }
   

}
