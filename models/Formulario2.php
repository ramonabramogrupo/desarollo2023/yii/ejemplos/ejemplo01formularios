<?php

namespace app\models;

/**
 * Description of Formulario2
 *
 * @author ramon
 */
class Formulario2 extends \yii\base\Model{
    public string $nombre=""; 
    public string $poblacion="";
    public string $color="";
    
    public function attributeLabels(): array {
        return [
            "nombre" => "Nombre del cliente",
            "poblacion" => "Poblacion del cliente",
            "color" => "Color elegido"
        ];
    }
    
    public function rules(): array {
        return [
          [['nombre','poblacion','color'],'required'],  
        ];
    }
    
    /**
     * poblaciones que voy a tener disponibles para rellenar el campo poblacion
     * @return array 
     */
    public function poblaciones(): array{
        return[
          "santander" => "SANTANDER" ,
          "torrelavega" => "TORRELAVEGA",
          "isla" => "ISLA"
        ];
    }
    
    public function colores():array{
        return [
          "rojo","azul","verde","amarillo"  
        ];
    }
    

}
