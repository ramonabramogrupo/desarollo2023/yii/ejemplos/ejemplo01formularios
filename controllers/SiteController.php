<?php

namespace app\controllers;

use app\models\Formulario1;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use const YII_ENV_TEST;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionEjercicio1() {
        $model = new Formulario1();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {

                return $this->render("solucion1", [
                            "model" => $model,
                ]);
            }
        }

        return $this->render('ejercicio1', [
                    'model' => $model,
        ]);
    }

    public function actionEjercicio2() {
        $model = new \app\models\Formulario2();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render("solucion1", [
                            "model" => $model
                ]);
            }
        }

        return $this->render('ejercicio2', [
                    'model' => $model,
        ]);
    }

    public function actionEjercicio3() {
        $model = new \app\models\Formulario3();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render("solucion1", [
                            "model" => $model,
                ]);
            }
        }

        return $this->render('ejercicio3', [
                    'model' => $model,
        ]);
    }

    public function actionEjercicio4() {
        $model = new \app\models\Formulario4();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render("solucion4", [
                            "model" => $model,
                ]);
            }
        }

        return $this->render('ejercicio4', [
                    'model' => $model,
        ]);
    }

    public function actionEjercicio5() {
        $model = new \app\models\Formulario5();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {

                return $this->render('solucion5', [
                            "model" => $model
                ]);
            }
        }

        return $this->render('ejercicio5', [
                    'model' => $model,
        ]);
    }

    public function actionEjercicio6() {
        /*
         * creo un modelo nuevo
         */
        $model = new \app\models\Formulario6();

        // cargo los datos del formulario
        if ($model->load(Yii::$app->request->post())) {
            // valido los datos
            if ($model->validate()) {
                // muestro el resultado
                return $this->render("solucion6", [
                            "model" => $model
                ]);
            }
        }

        return $this->render('ejercicio6', [
                    'model' => $model,
        ]);
    }

    public function actionEjercicio7() {
        $model = new \app\models\Formulario7();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render("solucion7",[
                   "model" => $model, 
                ]);
            }
        }

        return $this->render('ejercicio7', [
                    'model' => $model,
        ]);
    }

}
